from confluent_kafka import Producer
import random

p = Producer({'bootstrap.servers': "192.168.50.95:9092"})

some_data = []

for i in range(10):
    num = random.randint(2, 50)
    some_data.append('hello_{}'.format(num))

    # msg = 'hello_world_{}'.format(num)

    # some_data.append(bytes(msg, 'utf-8'))


# producer('test-topic', bytes(msg, 'utf-8'))


def delivery_report(err, msg):
    """ Called once for each message produced to indicate delivery result.
        Triggered by poll() or flush(). """
    if err is not None:
        print('Message delivery failed: {}'.format(err))
    else:
        print('Message delivered to {} [{}]'.format(
            msg.topic(), msg.partition()))


for data in some_data:
    # Trigger any available delivery report callbacks from previous produce() calls
    p.poll(0)

    # Asynchronously produce a message. The delivery report callback will
    # be triggered from the call to poll() above, or flush() below, when the
    # message has been successfully delivered or failed permanently.
    p.produce('test-topic', data.encode('utf-8'), callback=delivery_report)

# Wait for any outstanding messages to be delivered and delivery report
# callbacks to be triggered.
p.flush()
