from confluent_kafka import Consumer, KafkaException, KafkaError


conf = {
    # "bootstrap-dev.kafka.kb.dewkul.me:80",
    'bootstrap.servers': "192.168.50.95:9092",
    'group.id': "test",
    # 'default.topic.config': {'auto.offset.reset': "earliest"},
    # 'receive.message.max.bytes': 10000000000,
    # 'ssl.key.location': "ca.cert",
    # 'ssl.key.password': "ca.password"
}

c = Consumer(conf)

c.subscribe(['test-topic'])

while True:
    msg = c.poll(1.0)

    if msg is None:
        continue
    if msg.error():
        print("Consumer error: {}".format(msg.error()))
        continue

    print('Received message: {}'.format(msg.value().decode('utf-8')))

c.close()
